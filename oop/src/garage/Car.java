package garage;

public class Car extends Vehicle{
    String startRadio;

    public Car(String colour, int numberOfWheels, int numberOfObjectsInTrunk) {
        super(colour, numberOfWheels);
        this.numberOfObjectsInTrunk = numberOfObjectsInTrunk;
        startRadio = "the song";
    }

    int numberOfObjectsInTrunk;

    public int peekInTrunk() {

        return numberOfObjectsInTrunk;
    }
    public void StartRadio()
    {
        System.out.println(startRadio);
    }

    @Override
    public boolean isMoving() {
        return true;
    }

    @Override
    public boolean Move() {
        return false;
    }

    @Override
    public boolean Stop() {
        return false;
    }
}
