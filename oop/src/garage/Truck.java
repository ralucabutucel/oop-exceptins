package garage;

public class Truck  extends Car {
    public Truck(String colour, int numberOfWheels, int numberOfObjectsInTrunk) {
        super(colour, numberOfWheels, numberOfObjectsInTrunk);
    }

    @Override
    public int peekInTrunk() {
        if(numberOfObjectsInTrunk <= 10)
        {
            numberOfObjectsInTrunk++;
        }
       else { System.out.println("There are at least 10 objects in the trunk; ready for delivery!");}

        return super.peekInTrunk();
    }

}
