package garage;

public class Exceptions  implements Vehiclespec {

    public class AlreadyMovingException extends RuntimeException{

    }
    public class AlreadyStoppedException extends RuntimeException{

    }

    @Override
    public boolean isMoving() {
        return true;
    }

    @Override
    public boolean Move() {

        if(isMoving())
        {
            throw new AlreadyMovingException();
        }
        else
            return false;

    }

    @Override
    public boolean Stop() {
        if(isMoving())
        {
            throw new AlreadyStoppedException();
        }

        return false;
    }
}
